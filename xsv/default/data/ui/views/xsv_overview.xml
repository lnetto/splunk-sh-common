<dashboard stylesheet="xsv.css" hideEdit="true">
<label>Overview</label>
<row>
<html>
<div> <img src="/static/app/xsv/images/Scianta_Analytics_150.png" style="display:inline-block;"/>
  <h2 style="display:inline-block;padding-left:20px;color:#0069C2;font-size:20px;font-weight:bold;">Extreme Search Visualization</h2>
</div>
<br />
<p> <span class="head2">Introduction</span> </p>
<p>Extreme Search Visualization (XSV), is designed as a "helper" app for Scianta Analytics' Extreme Search for Splunk. Co-developed by Scianta Analytics and Splunk Inc., Extreme Search (XS) is now part of the Splunk App for Enterprise Security (ES) and will soon be part of Splunk Enterprise.  XSV provides a robust set of tools to help you create, manage and explore Extreme Search knowledge objects. Additionally, XSV provides  valuable documentation about Extreme Search beyond what is included in the app.</p><br />
<div><p class="xsv-inset"><strong>NOTE: </strong>XSV requires Extreme Search version 6.0.6 or later, which is included in ES version 3.3.1 and later. Note that it is not necessary to use the Extreme Search Visualization application or the Splunk App for Enterprise Security to use Extreme Search. Since Extreme Search is implemented as a set of extensions to Splunk's search language, once it is installed, it is available to any Splunk app.</p></div>
<p class="head2">What is Extreme Search?</p>
<p>The <strong>Extreme Search Engine</strong> is an integrated collection of powerful cognitive-computing-based analytics functions engineered for speed and transportability. The suite provides fast, flexible, and comprehensive statistical reasoning, predictive analytics and conceptual query capabilities in any computing environment. <strong>Extreme Search for Splunk</strong> is an implementation of the Scianta Extreme Search Engine which is engineered specifically for Big Data using the Splunk platform. The engine is optimized to run multiple, parallel threads on each Splunk indexer and is compiled to operate natively in Linux, Windows or Mac OS. This allows <strong>Extreme Search</strong> to deliver extraordinary performance, even on gigantic data sets.</p>
<p>Extreme Search is, first and foremost, a powerful cognitive computing engine. It is designed to "think" the way you think and to "understand" concept-based queries written in natural language.</p>
<br />
<p> <span class="head4">Converse with your data™</span> </p>
<p> Splunk is arguably the world's most powerful Big Data engine. <strong>Extreme Search</strong> transforms Splunk into the world's most powerful
  human-centered, cognitive computing platform for Big Data. Extreme Search understands
  concepts in the same way you understand them. This allows you to converse
  with your data the same way you converse with your co-workers, using powerful qualitative
  concepts in place of arcane quantitative parameters. </p>
<p>Why is this important? Because it allows regular users to gain deep insight from their data
  simply by asking questions in a way they understand.</p>
  <br />
<p class="head2">The Value Proposition in Enterprise Security</p>
<p>In addition to supporting qualitative expression  in Splunk searches, Extreme Search is capable of periodically refining the meaning of concepts based on a changing environment. Splunk uses this capability to implement <strong>Dynamic Thresholding.</strong> In the Splunk App for Enterprise Security, this allows the use of semantic terms as thresholds, in place of hard-coded numerical values. For example, a correlation search in ES might produce a notable event if the number of infected hosts is "high". Prior to Extreme Search, that search contained some arbitrary value (200, for example) as a threshold value for a "high number of infected hosts". Is 200 an appropriate &quot;high&quot; threshold value? That depends entirely on the environment.</p>
<p>One of the challenges in implementing a complex, powerful Splunk app like ES has been the time it takes to review every correlation search  in collaboration with a customer subject matter expert, replacing these arbitrary hard-coded threshold parameters with values that make sense for the organization. With Extreme Search, the correlation search might simply look for values that are "above low". Then a simple search can map the concept "low" to the organization's actual data. This has enabled a very real acceleration of time-to-value when deploying the Splunk App for Enterprise Security. ES can now be customized for a complex enterprise environment in hours instead of weeks.</p>
<p>Extreme Search also provides a very high level of granularity in setting thresholds. The notion of a "high number of login attempts" is unlikely to be the same on a Monday morning at 9AM as it would be on a Sunday at 2AM. Extreme Search can classify the context for number of login attempts based on any secondary control parameter: time of day; day of week; network type; product category; employee role, etc.. This ability to support <strong>Highly Granular, User-Defined, Dynamic Thresholding</strong> is one of the important reasons Extreme Search is now included with the Splunk App for Enterprise Security.</p>
<br />
<p> <span class="head2">Command Categories</span> </p>
<p>Extreme Search commands can be divided into three operational categories:</p>
<ul>
  <span class="head5"><li>Conceptual Search</li></span> Qualitative, concept-based data exploration. 
  <span class="head5"><li>Statistical Reasoning</li></span> Powerful regression and correlation tools for data
  analysis.
  <span class="head5"><li>Predictive Analytics</li></span> State-of-the-art machine learning tools.
</ul>
<p>The <a href="command_reference">Command Reference</a> documentation page of this app describes each command in each of these three
  categories.</p>
  <br />
<p> <span class="head4">Conceptual Search</span> </p>
<span class="head4">Terminology</span>
<p>To understand Extreme Search's concept-based search, it is useful to understand some
  terminology used in the product and its documentation:</p>
<ul>
  <li><strong>Concept: </strong>An idea represented by a<strong> descriptive semantic term</strong>. In
    Extreme Search, these terms are usually user defined as part of a <strong>Context . </strong><em>Tall, Short, Fast and Slow are </em><strong>Semantic Terms</strong> used to describe <strong>Concepts</strong>.</li>
  <li><strong>Context: </strong>A collection of terms that form a conceptually coherent view
    of a knowledge domain. <em>Height</em> might be a <strong>Context</strong> comprised of
    the <strong>Terms</strong> <em>Tall</em> and <em>Short</em>. <em>Speed</em> might be a <strong>Context</strong> comprised of the <strong>Terms</strong> <em>Fast</em>, <em>Typical</em> and <em>Slow</em>.</li>
  <li><strong>Semantic </strong><strong>Term: </strong>A linguistic representation of
    a <strong>Concept</strong>. The semantic term <em>Tall</em> might be used to represent the <strong>Concept</strong> of people of large physical stature as a part of the <strong>Context</strong><em> Height</em>.</li>
</ul>
  
<p>For an introduction to conceptual search, click on "Learn More About Conceptual Search". </p>
<p>To discover more about conceptual search, its role in human-based cognitive computing and
  the science upon which Extreme Search is based, click on "Deep Dive" to download <strong>Volume 1: Principles of Cognitive Computing</strong>, Earl Cox, Chief Scientist,
  Scianta Analytics. (PDF)</p>
<p> <a class="btn btn-xtreme" href="concept_intro" target="_blank"> <span class="icon-play"></span> Learn More About Conceptual Search </a> <a class="btn btn-xtreme" href="http://www.sciantaanalytics.com/sites/default/files/SA-IDXR-REFERENCE-V01_Cognitive_Computing.pdf" target="_blank"> <span class="icon-play"></span> Deep Dive </a> </p>
<br />
<p> <span
          class="head4">Statistical Reasoning</span> </p>
<p>Scianta Extreme Search includes powerful statistical reasoning functions that facilitate
  analysis of very large data sets quickly and intuitively within Splunk. Extreme Search
  supports the following categories of Statistical Reasoning functions:</p>
<ul>
  <span
          class="head5">
  <li>Linear Regression</li>
  <li>Non-linear Regression</li>
  <li>Auto Regression</li>
  <li>Correlation</li>
  </span>
</ul>
<p>Learn more about Statistical Reasoning in Extreme Search in the <a
          href="command_reference">Command Reference.</a></p>
          <br />
<p> <span
          class="head4">Predictive Analytics</span> </p>
<p>The Scianta Extreme Search engine is part of the Scianta Analytics Cognitive
  Computing Suite. Our Cognitive Computing Suite delivers industry-leading, proprietary
  cognitive modeling, machine learning and predictive analytics to the world of Big Data.
  Extreme Search for Splunk includes concept-based predictive analytics functions
  that run natively, within Splunk.</p>
<p>Learn more about Predictive Analytics in Extreme Search for Splunk in the <a
          href="command_reference">Command Reference.</a></p>
          <br />
<p> <span
          class="head2">Using Extreme Search</span> </p>
<p>We encourage you to review the Extreme Search <a
          href="command_reference">Command Reference.</a> Taken together, these commands deliver to
  Splunk users the power of Scianta Analytics' cognitive-computing-based conceptual search,
  statistical reasoning and predictive analytics technology natively, within the Splunk
  platform. As a query processor, the components can also be combined and used as a data
  filter to collect, filter, and rank information based on the qualitative semantics
  associated with each data element. Because Extreme Search is tightly integrated with Splunk,
  results are delivered alongside the results of native Splunk search commands.</p>
<p>Extreme Search is implemented as a set of extensions to Splunk's Search Processing Language (SPL). All Extreme Search functions may be entered as commands directly within the Splunk Search bar, or within scheduled searches and reports.
  Results are  displayed within the Splunk web interface in the same manner as any other Splunk search. It is not necessary to use a specific Splunk app or an external
  interface to take advantage of conceptual search. Some Scianta Analytics cognitive computing suites,
  such as Scianta Analytics Extreme Vigilance™ , execute Extreme Search functions and display results
  through their own user interfaces. The Splunk App for Enterprise Security has integrated Extreme Search qualitative expression within many ES searches, reports and dashboards. Please see the documentation for these systems to learn
  how they work with Extreme Search for Splunk. Documentation on the use of Extreme Search within the Splunk App for Enterprise Security is <a href="http://docs.splunk.com/Documentation/ES/3.3.1/User/ExtremeSearch" target="_blank">available  online here.</a></p>
  <br />
<p> <span class="head2">Extreme Search Architectural Hierarchy</span></p>
<p>Knowledge objects in Extreme Search follow this organizational hierarchy:</p>
<ul>
<li><strong>App</strong> </li>
        Any Splunk App may contain Extreme Search knowledge objects.
    	<li><strong>Container </strong></li>
    	An App may contain one or more XS Containers.  A Container is a special kind of CSV-formatted lookup file designed to contain one or more Contexts. Containers are generally transparent to the XS knowledge object hierarchy. They can best be thought of as a collection of Contexts.
    	<li><strong>Context </strong></li>
    	Containers contain one or more Contexts. A Context is a semantically coherent set of Concepts.
         All of the Concepts assembled into a particular Context must apply to the same knowledge domain. The Concepts Tall and
          Short may share a Context.  Tall and Fast may not.
    	<li><strong>Context Class</strong> </li>
    	Contexts may be classified based upon the value of a secondary field. If a Context contains Concepts associated with Network Latency, you might define a Context Class for each network_type and a Default Class that applies to all network_types. The Classes would be named based upon the potential values of the network_type field. The value of the network_type field can then be used to select the appropriate Context to be used in your search. 
<li><strong>Concept</strong> </li>
A Concept is a descriptive semantic term. It is represented as a two-dimensional array of points. The X axis corresponds to the value of the field in question. The Y axis represents the membership of that value in the Concept, stored as a value from 0 to 1.
</ul>
<br />  <p
        align="center"><img src="/static/app/xsv/images/xs_hierarchy_800.png" width="800" height="698" align="center" alt=""/></p>
<br /><br />
<p>The XSV <a href="/app/xsv/xsv_explore_context">Context Explorer</a> dashboard  allows you to visually navigate Extreme Search knowledge objects  using this hierarchy.</p><br />
<p class="xsv-inset">Extreme Search, as packaged with the Splunk App for Enterprise Security, is a Splunk Supporting Add-on called &quot;Splunk_SA_ExtremeSearch&quot; at etc/apps/Splunk_SA_ExtremeSearch. If installed as a stand-alone app, you'll find it at etc/apps/xtreme.</p><br />
<p><span class="head2">Documentation and Help</span> </p>
<p>Detailed information about each Extreme Search XS and XSV command is provided within your Splunk
  environment by Splunk's interactive help system. In addition, the Extreme Search Visualization application
  view includes <strong>Help</strong> pages like this one within the application view. Some
  important <strong>Help</strong> pages include:</p>
<ul>
  <li><a href="concept_intro"> Introduction to Conceptual Search</a></li>
   <li><a href="hedges"> Hedges and Synonyms</a></li>
  <li><a href="using_xsv"> Using Extreme Search Visualization</a></li>
  <li><a href="command_reference"> Extreme Search Command Reference</a></li>
  <li><a href="http://www.sciantaanalytics.com/?q=learn" target="_blank"> Scianta Online Docs: Extreme Search Visualization</a></li>
  <li><a href="http://docs.splunk.com/Documentation/ES/3.3.1/User/ExtremeSearch" target="_blank"> Splunk Online Docs: Extreme Search</a></li>
</ul>
<br />
<p>VERSION: 20150819.1<br />
      Custom application development for Splunk by <a href="http://www.concanon.com/splunkdev"
          target="_blank"> Concanon LLC.</a><br />
	Some portions of Extreme Search for Splunk are patented under US Patent 9,087,090.<br />
Copyright 2015 Scianta Analytics LLC. All Rights Reserved.</p>
</html>
</row>
</dashboard>
